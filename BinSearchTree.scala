/*
CSE262
Chris Mallalieu
cpm220
BinSearchTree
Program #2
*/

import scala.io.Source
import scala.collection.Map

case class Node ( //Node class to hold fields
  var key : String = null,
  var value : String = null,
  var left : Node = null,
  var right : Node = null,
)

object BinSearchTree extends App {
  val tree = Node("abc", "1", Node("aaa", "2", null, null), Node("bbb", "3", null, null))


  def find(key:String, tree:Node) : Option[String] =  tree match { //returns the value given the key
    case Node(matchKey:String, matchVal:String, _, _) if matchKey == key => Option(matchVal) //if key is found, return value
    case Node(matchKey:String, matchVal:String, leftNode, _) if matchKey > key => find(key, tree.left) //if key is greater than the current node, go left
    case Node(matchKey:String, matchVal:String, _, rightNode) if matchKey < key => find(key, tree.right) //if key is less than the current node, go right
    case _ => None //if the key is not in the tree, return none
  }
    
  def add(key:String, value:String, tree:Node) : Option[String] = tree match { //adds nodes to the tree
    case Node(matchKey:String, matchVal:String, _, _) if matchKey == key => None //if the node already exists, return none
    case Node(matchKey:String, matchVal:String, leftNode, _) if matchKey > key && leftNode == null => tree.left = Node(key, value, null, null) ; Some (key) //add node to the left side
    case Node(matchKey:String, matchVal:String, _, rightNode) if matchKey < key && rightNode == null => tree.right = Node(key, value, null, null) ; Some (key) //add node to the right side
    case Node(matchKey:String, matchVal:String, leftNode, _) if matchKey > key && leftNode != null => add(key, value, tree.left) //recurse through the tree to find the right node to add to 
    case Node(matchKey:String, matchVal:String, _, rightNode) if matchKey < key && rightNode != null => add(key, value, tree.right) // ^
  }

  if (args.length > 0) { //read in args
    val filterPattern = "\"(.*?)\"|([^\\s]+)".r //filter out unwanted characters
    val wordList = io.Source.fromFile(args(0)).getLines.flatMap(filterPattern.findAllIn).toList //reads in text file
    val singleWordList = io.Source.fromFile(args(1)).getLines.flatMap(filterPattern.findAllIn).toList //reads in text file 
    var i = 0
    for (word <- wordList) {
      if (i % 2 == 0 && i + 1 < wordList.size) { //imports the first word followed by the next word as a key/value pair
        add(wordList(i), wordList(i + 1), tree) //add imported text into tree
      }
      i =  i + 1
    }
    for (singleWord <- singleWordList) { //finds value given a key 
      val finalAnswer = find(singleWord, tree)
      finalAnswer match {
        case Some(s:String) => println(s) //formatted print
        case None => println("NONE") //formatted print
      }
    }
  }
}