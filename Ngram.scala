object Ngram {

/*
Chris Mallalieu
cpm220
CSE262
Program #1
Next word probability Calculator
*/

import scala.io.Source
import scala.collection.mutable.Map

var wordList : List[String] = null //initialize empty list
var wordMap = scala.collection.mutable.Map[Tuple2[String, String], Int]() //initializs the map
var tuple1: String = _ //initializes empty strings 
var tuple2: String = _

  def main(args: Array[String]): Unit = {
    
    if (args.length > 0) {
      val individualWord = """([A-Za-z])+""".r //regex expression to split words
      wordList = io.Source.fromFile(args(0)).getLines.flatMap(individualWord.findAllIn).map(_.toLowerCase).toList //reads in file as lowercase map
    } else {
        println ("please enter a text file argument") //handles if there is no argument
    }

    for (i <- 0 to wordList.length - 2) { //creates tuples
      for (j <- i + 1 to i + 1) {
        tuple1 = wordList(i) //first tuple component
        tuple2 = wordList(j) //second tuple component
        if (!wordMap.contains(tuple1, tuple2)) {
          wordMap += ((tuple1, tuple2) -> 1)
        } else {
          wordMap += (tuple1, tuple2) -> (wordMap(tuple1, tuple2) + 1)
        }
      }
    }
    val distinctWords = wordList.distinct //returns a list with only distinct words
    for (word <- distinctWords) { //prints out the final print statement
      if (word != distinctWords.last) {
        println (word + " : " + mostLikelyNextWord(word))
      }
    }
  }

  def countAll(word:String): Int = { //counts all the words that follow 
    var filterList = wordList.filter (_ == word)
    var count = filterList.length 
    if (wordList(wordList.length - 1) == wordList.last && wordList.contains(word)) {
    count = count
    }
    count
  }

  def p1(of:String, given:String): Double = { //returns probibility of a word given another word
    if (countAll(given) != 0 || countAll(of) != 0) {
      wordMap(given, of).toDouble / countAll (given).toDouble
    } else {
      0.0
    }
  }

  def mostLikelyNextWord(given:String) : String = { //returns the most likely next word
    var givenMap = wordMap.filterKeys(_._1 == given)
    var prob = 0.0
    val tupleList = (givenMap.keys.toList)
    var myWord = ""
    for (key <- tupleList) //climbs through the loop
      if (p1(key._2, given) > 0) {
        prob = p1(key._2, given)
        myWord = key._2
      }
      myWord //return myWord
  }
}
